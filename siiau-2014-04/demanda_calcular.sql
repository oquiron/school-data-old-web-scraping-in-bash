# quiron 2014-05
# Calcula la demanda del 2014A
\! echo -e "Inicio "
\! date

\! echo "0. Cargando calendarios"
#source inscripcion-201120.sql;
#source inscripcion-201210.sql;
#source inscripcion-201220.sql;



########################################################################
\! echo "6. Agregando el campo estatus a trayectoria"
########################################################################

# Agrega el campo estatus
create temporary table alumno_trayectoria_edo as
select t.*,edo as estatus
from alumno_trayectoria as t , siiau_alumno_carrera
where t.codigo = siiau_alumno_carrera.codigo
    and t.carrera = siiau_alumno_carrera.carrera
;

drop table alumno_trayectoria;
create table alumno_trayectoria as 
select distinct * from alumno_trayectoria_edo
order by carrera,codigo,semestre;



# alumno abanico con estatus
create temporary table alumno_abanico_edo as
select t.*,edo as estatus
from alumno_abanico as t , siiau_alumno_carrera
where t.codigo = siiau_alumno_carrera.codigo
    and t.carrera = siiau_alumno_carrera.carrera

;

\! echo "Creando la tabla alumno_abanico con estatus"
drop table if exists alumno_abanico;
create table alumno_abanico as
select distinct * from alumno_abanico_edo
order by semestre;


########################################################################
\! echo "1. Agregando turno a siiau_inscripcion"
########################################################################
\! echo "   Creando la tabla alumno_turno"


# Turno del alumno el 2014A
# -----------------------------------------------------
# El turno debería ser un campo en la tabla inscripcion
drop table if exists alumno_turno;
create table alumno_turno as
select codigo,carrera,calendario, matutino, vespertino,if(matutino >= vespertino,'MAT','VES') as turno 
from (
  select codigo,carrera,calendario, clave, sum(if(ini<1400,1,0)) as matutino, sum(if(ini<1400,0,1)) as vespertino
  from (select codigo,carrera,calendario,clave,ini 
    from siiau_inscripcion group by codigo,carrera,calendario,clave) 
    as siiau_inscripcion 
  #where codigo='212348088'
  group by codigo,carrera,calendario
) as turnos
where calendario='2014A'
group by codigo,carrera
;


# Agrega el turno a siiau_inscripcion
# 3 minutos, subio a 12 minutos
\! echo "   siiau_inscripcion_turno"

create temporary table siiau_inscripcion_turno
select a.*,turno
from siiau_inscripcion as a, alumno_turno as t
where a.codigo = t.codigo
   and a.carrera = t.carrera
;


\! echo "   Creando tabla siiau_inscripcion con turno"
drop table siiau_inscripcion;
create table siiau_inscripcion as 
select * from siiau_inscripcion_turno
;

########################################################################
\! echo "2. Creando la demanda_alumno"
########################################################################
# Demanda con turno ( Esta es la buena)
# OJO: A veces la primer consulta da una tabla vacia
\! echo "   OJO: Creando la tabla alumno_demanda_con_turno"

set @num:=0;
set @codigo:='';
#drop table if exists   alumno_demanda_con_turno;
#create temporary table alumno_demanda_con_turno as
drop table if exists alumno_demanda;
create table alumno_demanda as
select numerados.codigo,
    numerados.carrera,
    estatus,
    turno,
    clave,
    semestre,
    semestre_ultimo,
    prioridad
from(
   select  estatus,alumno_abanico.codigo,alumno_abanico.carrera,/*turno,*/clave,semestre,semestre_ultimo,
     if(@codigo<>alumno_abanico.codigo,@num:=0,''),
     @num:=@num+1 as prioridad,@codigo:=alumno_abanico.codigo  
   from alumno_abanico
   where ( estatus like 'ACTIVO%' or estatus like '%ARTICULO 34%' )
  ) as numerados 
  ,alumno_turno
where numerados.codigo = alumno_turno.codigo
  and numerados.carrera = alumno_turno.carrera
  and prioridad <= 7

  #and ( estatus like 'ACTIVO%' or estatus like '%ARTICULO 34%' )
   
  #and numerados.carrera='RHU' 
  #and numerados.codigo='206572863'
  #limit 40

;


# OJO: Revisar cuántos quedan después del filtro.


#create table alumno_demanda as
#select * from alumno_demanda_con_turno;

/*
La creación des estatus se hace antes de crear demanda
# Demanda con estatus
\! echo "   Creando alumno_demanda con estatus"
drop table if exists alumno_demanda;
create table alumno_demanda
select d.*, a.edo as estatus
from alumno_demanda_con_turno as d, siiau_alumno_carrera as a
where d.codigo = a.codigo
  and d.carrera = a.carrera
;
*/


########################################################################
\! echo "3. Calculando reprobacion en materia_inscripcion"
########################################################################


# Indice de reprobación por materia
\! echo "    materia_reprobacion_calendario"
drop table if exists materia_reprobacion_calendario;
create table materia_reprobacion_calendario as
select carrera,clave,calendario,alumnos,aprobados,alumnos-aprobados as reprobados, 
(alumnos-aprobados)/alumnos as reprobacion
from
(
select carrera,clave,calendario,
    count(distinct codigo) as alumnos, 
    count(if(creditos>0,1,null)) as aprobados
from calificacion 
#where clave='AD120' and carrera ='AFS' 
group by carrera,clave,calendario 
order by carrera,clave,calendario
) as lelolero
;



# Indice de reprobación por materia ( sin calendarios )
\! echo "    Creando vista materia_reprobacion"
drop table if exists materia_reprobacion;
create table materia_reprobacion as

select clave,inscritos,reprobados, avg(reprobados/inscritos) as reprobacion from 
  (select clave,sum(reprobados) as reprobados,sum(alumnos) as inscritos
  from materia_reprobacion_calendario
  where  (calendario='2013B' or calendario='2013A' or calendario='2012B')
   group by clave
  ) as lerolero
group by clave

;


\! echo "   materia_inscripcion_sin_reprobacion"
drop table if exists materia_inscripcion_sin_reprobacion;
create temporary table materia_inscripcion_sin_reprobacion as
select calendario,clave,carrera,turno, count(codigo) as alumnos
from siiau_inscripcion 
group by calendario,clave,carrera,turno;

\! echo "   Creando tabla materia_inscripcion"
drop table if exists materia_inscripcion;
create table materia_inscripcion as
select calendario,i.clave,carrera,turno,alumnos,reprobacion as reprobacion, 
       if(reprobacion is null, 0,round(alumnos*reprobacion)) as reprobados
from materia_inscripcion_sin_reprobacion as i
    left join materia_reprobacion as r
on i.clave = r.clave 
where calendario='2014A'

;


###################################
# Demanda por materia
###################################


########################################################################
\! echo "4. Creando demanda_materia"
########################################################################
# La primera vez que se ejecuta no se crea bien
\! echo "   OJO: Creando la tabla materia_demanda de activos y 34s"

drop table if exists materia_demanda;
create table materia_demanda as
select clave,carrera,turno,count(codigo) as alumnos
from alumno_demanda 
where estatus like 'ACTIVO%'
  or estatus like '%ARTICULO 34%'
group by clave,carrera,turno 
;



\! echo "   materia_demanda -1"

drop table if exists materia_demanda_repetidores;
create temporary table materia_demanda_repetidores as
select distinct d.clave,d.carrera,d.turno,d.alumnos,
      if(reprobados is null,0,reprobados) as repetidores, 
      d.alumnos + if(reprobados is null,0,reprobados) as demanda
from materia_demanda as d 
left join materia_inscripcion as  i
on   d.clave = i.clave 
 and d.carrera = i.carrera
 and d.turno = i.turno
;


######################################
# Calcula los retenidos

create temporary table if not exists requisitos_arreglo  engine=InnoDB as
select clave,carrera,
substr(prerrequisitos,2,4) as area1,substr(prerrequisitos,7,5) as requisito1,
substr(prerrequisitos,13,4) as area2,substr(prerrequisitos,18,5) as requisito2,
substr(prerrequisitos,24,4) as area3,substr(prerrequisitos,29,5) as requisito3,
substr(prerrequisitos,35,4) as area4,substr(prerrequisitos,40,5) as requisito4

    from siiau_materia_carrera

;



\! echo "   materia_demanda 0"
drop table if exists   requisitos_0;
create temporary table requisitos_0 as
 select d.*,requisito1,requisito2
 from materia_demanda_repetidores as d
     left join
     requisitos_arreglo as r
      on d.clave = r.clave
     and d.carrera = r.carrera
 ;



\! echo "   materia_demanda 1"

drop table if exists   requisitos_1;
create temporary table requisitos_1 as 
select r.*,if(d.repetidores is null,0,d.repetidores) as retenidos1
from requisitos_0 as r
 left join
 materia_demanda_repetidores as d
 on d.clave = requisito1
 and    r.carrera = d.carrera
 and    r.turno = d.turno
;


\! echo "   materia_demanda 2"
drop table if exists   requisitos_2;
create temporary table requisitos_2 as 
select distinct r.carrera,r.clave,r.turno,r.alumnos,r.repetidores,requisito1,retenidos1,requisito2,
    if(d.repetidores is null,0,d.repetidores) as retenidos2
from requisitos_1 as r
 left join
 materia_demanda_repetidores as d
 on d.clave = requisito2
 and d.carrera = r.carrera
 and    r.turno = d.turno
;


\! echo "   materia_demanda"
drop table if exists materia_demanda;
create table materia_demanda as
select *, alumnos + repetidores - retenidos1 - retenidos2 as demanda
from requisitos_2
order by carrera,clave,turno
;


########################################################################
\! echo "5. Calculando la demanda por tendencia"
########################################################################

# Solo el úlmito calendario se extae de siiau_inscripcion, el resto de calificacion
drop table if exists materia_tendencia;
create table materia_tendencia as
select distinct 2011A.clave, 2011A, 2011B, 2012A, 2012B, 2013A, 2013B, 2014A
from 
( select materia.clave, ifnull(total,0) as 2011A from
 (
    select distinct clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2011A'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2011A
,
( select materia.clave, ifnull(total,0) as 2011B from
 (
    select clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2011B'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2011B


,
( select materia.clave, ifnull(total,0) as 2012A from
 (
    select clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2012A'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2012A


,
( select materia.clave, ifnull(total,0) as 2012B from
 (
    select clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2012B'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2012B


,
( select materia.clave, ifnull(total,0) as 2013A from
 (
    select clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2013A'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2013A

,
( select materia.clave, ifnull(total,0) as 2013B from
 (
    select clave 
    from materia_carrera 

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from calificacion 
    where calendario ='2013B'
    group by clave,calendario 
 )  as inscritos
on (materia.clave = inscritos.clave ) 
) as 2013B

,
(select materia.clave, ifnull(total,0) as 2014A from
 (
    select clave
    from materia_carrera

 ) as materia
left join
 (
    select distinct clave,count(distinct codigo) as total
    from siiau_inscripcion
    where calendario ='2014A'
    group by clave,calendario
 )  as inscritos
on (materia.clave = inscritos.clave )
) as 2014A

where 2011A.clave = 2011B.clave
and 2011A.clave = 2012A.clave
and 2011A.clave = 2012B.clave
and 2011A.clave = 2013A.clave
and 2011A.clave = 2013B.clave
and 2011A.clave = 2014A.clave
;



\! echo -e "Fin: "
\! date


