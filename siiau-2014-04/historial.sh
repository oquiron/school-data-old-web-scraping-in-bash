#!/bin/bash
# Oscar Perez 2008 0304
# Actualización 2014 04
# Registro: curso(crn,materia,profesor) profesor(nombre, codigo)  alumno(codigo, nombre, carrera) materia(clave, depto, creditos) registro(alumno,crn)
# Oferta: curso(crn,materia,profesor)
# calificacion (alumno, crn, calendario) alumno (estatus)
# TAREA:
#	- conseguir una fuente de codigos de alumno confiable
#	- eliminar los campos entre comillas que tengan comas sed s/\"[^,]*,[^\"]*\"// 
#	- Crear la tabla de codigos de carrera, al procesar el historial 
#	- Cambiar al generar estudiantes, el edo por las claves de edo.




INSCRIPCION=true
CALIFICACIONES=false


# apt install html2text

calendarios="200890 200810 200790 200780 200720 200710 200690 200680 200620 200610 200580 200520 \
200510 200480 200420 200410 200380 200320 200310 200280 200220 200210 200120 200110 200020 200010 199990 199920 199910 199820 \
199810 199720 199710 199620 199610 "

calendarios="201420 201410 201320 201310 201220 201210 201120 201110 201020 201010 200920 200910 200890 200810 200790 \
	200780 200720 200710 200690 200680 200620 200610 200520 200510 200420 200410"

#calendarios="201410 201320 201310 201220 201210 201120 \
# 201110 201020 201010 200920 200910 200820 200810 200720 200710 200620 200610 200520 200510 200420 200410 200320 200310 \
calendarios="200220 200210 200120 200110 200010 200020 199920 199910 199820 199810 199720 199710 199620 199610 199520 199510 199420 199410"

calendarios="201420 201410 201320 201310 201220 201210 201120 201110  201020 201010 200920 200910 200890 200810 200790 200780 200720 200710 200690 200680 200620 200610"

# Evaluación conacyt 2017
calendarios="201110 201120 201210 201310 201320 201410 201420 201510 201520 201610 201620 201710"
calendarios="201510 201520 201610 201620 201710 201720 201910"
calendarios="201920"

fecha=`date +%Y-%m%d`
tiempo=$(date +%s)

echo "Fecha" $fecha
#fecha=2014-0524

[ ! -d $fecha ] &&  mkdir $fecha


echo "Iniciando sesión..."
. conectar.sh &
HIJOPID=$!

sleep 10s
echo Conectado...

UA="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:15.0) Gecko/20100101 Firefox/15.0.1"

# =========================================================================
# Registro (Inscripcion)
#


#Descarga los inscritos a los cursos
if $INSCRIPCION ; then

echo "Descargando Inscripcion a cursos (Registro) $calendarios"
for ciclo in $calendarios ; do
	cicloAB=$( echo $ciclo | sed 's/\(....\)10/\1A/' | sed 's/\(....\)20/\1B/');
	echo -n "$cicloAB,"

        # Descarga el registro de estudiantes por cada calendario
        wget    --user-agent="$UA" --cookies=on --keep-session-cookies --load-cookies cookies.txt \
                http://siiauescolar.siiau.udg.mx/wse/sfpcons.general  -q -O - \
                --post-data="cup=C&ciclop=$ciclo&carrerap=X&deptop=X&secp=T&ordenp=CA&gpop=A&mostrarp=1000000&tipop=D" \
        | sed s/^/\"$cicloAB\",/ \
        | sed 1d \
	| sed 's/, / /' \
        > $fecha/inscripcion.csv

        #######################################
        # 2014-05-17
        # Descarga el registro de estudiantes en materias inactivas
        # (SIIAU tiene un error, no se descargan las materias inactivas en formato CSV)

wget  --user-agent="$UA" -  --cookies=on --keep-session-cookies --load-cookies cookies.txt \
   http://siiauescolar.siiau.udg.mx/wse/sfpcons.registro  -q -O - \
   --post-data="cup=C&ciclop=$ciclo&carrerap=X&deptop=X&secp=I&ordenp=CA&gpop=C&mostrarp=1000000&tipop=T" \
| tr '&' '\n' | tr '\n' ' ' | sed s/'TD>'/@/g | sed s/'2">'/@/ \
| tr '@' '\n'| grep -e 'codalump=\|CLAVE:\|[0-9][0-9][0-9][0-9] - ....\|SIZE="1">[A-Z]*</FONT' | sed s/$/\>/| sed s/'<[^>]*>'//g \
| while read uno dos tres; do

    if [[ "$uno"X == ?????????X ]]   ; then
    codigo=$uno

        continue
    fi

    if [ "$tres"X != ""X ] ; then
     ini=$uno
     fin=$tres
     continue
    fi

    if [ "$dos"X != ""X ] ; then
       clave=$dos
       continue
    fi

    carrera=$uno

    #echo $calendario $clave $ini $fin $codigo $carrera
    echo "insert into siiau_inscripcion values (\"$cicloAB\",null,\"$clave\",null,null,null,null,\"$ini\",\"$fin\",null,null,null,null,null,null,null,null,null,\"$codigo\",null,\"$carrera\");"
done > $fecha/inscripcion-${ciclo}-inactivas.sql
#######################################################


    # Crea el archivo registro.sql
    # ,NRC,CLAVE,MATERIA,SEC,CRED,DEPTO,INI,FIN,L,M,I,J,V,S,EDIF,AULA,PROFESOR,COD_AL,ALUMNO,CAR_AL
cat << EOF > $fecha/inscripcion-$ciclo.sql
        create table if not exists siiau_inscripcion (
                calendario char(10),
                nrc     char(10),
                clave   char(10),
                materia char(150),
                seccion char(2),
                creditos        int(2),
                departamento    char(200),
                ini     char(10),
                fin     char(10),
                L       char(10),
                M       char(10),
                I       char(10),
                J       char(10),
                V       char(10),
                S       char(10),
                edificio        char(20),
                aula    char(10),
                profesor        char(100),
                codigo  char(15),
                alumno  char(250),
                carrera char(10)

        );


EOF

    cat $fecha/inscripcion.csv \
	| sed 's/,\([0-9][0-9]*\)/,\"\1\"/g' | sed 's/^\([0-9][0-9]*\)/\"\1\"/g' \
        | sed s/^/"insert into siiau_inscripcion values ("/ \
        | sed s/$/");"/ \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/g \
        | sed s/,\)/,'""'\)/ \
	| sed s/"\"NO UNIVERSI"/"NO UNERVERSI"/ \
        >> $fecha/inscripcion-$ciclo.sql

done

echo
echo "Descarga completa de Registros $calendarios"
cat $fecha/inscripcion-*.sql  | cut -d, -f19 | tr '"' ' ' | sort   | uniq > $fecha/codigos_alumnos.csv
echo "$fecha/codigos_alumnos.csv creado"

fi




# ============================================================================
# Dictamenes
# poner en falso para probar los c�digos ya descargados en $fecha/codigos_alumnos.csv
if false ; then
	echo -n "Descargando los dictamenes de 1er ingreso: "
	## Tarea: Crear la tabla de alumno a partir de este dato

	for calendario in $calendarios ; do
		echo -n $calendario " "
		#wget --referer="http://s2.siiau.udg.mx/wse/guprepo.reportem?nombrep=SGRREPI"
		wget --referer="http://siiauescolar.siiau.udg.mx/wse/guprepo.reportem?nombrep=SGRREPI" \
		--user-agent="$UA" --cookies=on --keep-session-cookies http://148.202.1.55/dev60cgi/rwcgi60 \
		--post-data="report=SGRREPI.rep&p_centro=C&p_cicloadm=$calendario&p_carrera=&p_status=&p_sede=&server=rep60_MURPHY&destype=CACHE&desname=CACHE&paramform=NO&desformat=HTML&userid=readreport/reportxvo@prodes" \
		-q -O - # | html2text  | cut -b1-10 >> /tmp/dictamen


	done


cat /tmp/dictamen | sort | uniq | grep -v "="  > $fecha/codigos_alumnos.csv
echo
echo "Codigos bajados en $fecha/codigos_alumnos.csv ... "
fi

# =============================================================================
# Recupera el historial de alumnos utilizando los c�digos de alumnos obtenidos en el registro
#cat $fecha/registro.csv  | cut -d\; -f7 | sort | uniq > codigos_alumnos.csv

# quiron 1:

if $CALIFICACIONES ; then

cat << EOF >> $fecha/alumnos.sql

create table if not exists siiau_alumno_carrera (
        codigo varchar(12),
        nombre varchar(50),
        carrera varchar(250),
        edo varchar(100),
        ciclo_ingreso varchar(6),
        ciclo_ultimo varchar(6),
        centro varchar(250)


);

EOF

cat << EOF >> $fecha/calificaciones.sql

create table if not exists calificacion (
	    #carreraA varchar(5),	#  Carrera del alumno ( la otra es carrera de la calificacion )
        codigo varchar(12),
        carrera varchar(5), # esta columna no es redundante
        calendario varchar(6), # Alguno de estos datos debe ser redundante
        nrc varchar(10),
        clave varchar(6),  # es redudante, pero no existe una tabla "cursos"
        calificacion int,
        tipo varchar(15),
        creditos int, # permete averiguar si aprobo
        fecha date

);

EOF


#mkdir $fecha/alumnos
echo "recuperando el historial por alumno de siiau:"
	#>   $fecha/alumnos/$codigo_alumno.txt
#[ -f $fecha/alumnos.sql ] &&  rm $fecha/alumnos.sql
#[ -f $fecha/calificaciones.sql ] && rm $fecha/calificaciones.sql

for codigo_alumno in `cat $fecha/codigos_alumnos.csv` ; do

	echo -n $codigo_alumno
	# quiron 2:
	# Si el alumno ya fue descargado, continuar con el siguiente
	if grep $codigo_alumno $fecha/alumnos.sql > /dev/null; then
		echo -n ','
		continue;
	fi

	edo="INI"
    # Muestra el numero de alumno procesado
    echo -n '('$(wc -l $fecha/alumnos.sql  | cut -f1 -d' ' )'), '

	# quiron 3: OJO: COPIAR DE NUEVO ESTE CODIGO, ESTA MAL
	# reiniciar sesion si paso 30 minutos

	#if false ; then
	#if [ $(($tiempo + 1800)) -lt  $(date +%s) ] ; then
	#	tiempo=$(date +%s)
	#	echo  "Iniciando la sesi en SIIAU"
	#	wget --cookies=on --keep-session-cookies --save-cookies cookies.txt http://siiauescolar.siiau.udg.mx/wus/gu$
        #		--post-data="p_codigo_c=$usuario&p_clave_c=$contrasenia" -O /dev/null  -q
	#fi
	#fi

	# quiron 4: SIGLO XX
	wget --user-agent="$UA" --cookies=on --keep-session-cookies --load-cookies cookies.txt http://siiauescolar.siiau.udg.mx/wse/shpcoha.muestroHA \
        --post-data="codigop=$codigo_alumno&ordenp=NM" -q -O - |  sed s/"&.acute;"//g | html2text -width 1000 -nobs | tr _ ' ' | grep '|'  \
	| sed s/"SIGLO XX (PROCESOS"/"SIGLO XX PROCESOS"/ \
	| while read linea ; do

    # Esta linea se elimino porque creo que es del campo carreraA que ya no se usa
    # | sed s/"(\([^)]*\))"/"===\1==="/g \

		case $edo in
			# Lee los datos del estudante
			"INI" )

				campo=$(echo $linea | cut -c2-8)

				case $campo in
					"Cdigo: " )
						codigo=`echo $linea | cut -f2 -d' '`
						nombre=`echo $linea | sed s/.*Nombre://  | sed s/\'/' '/g`
						;;

					" Carrer")

                                                #carrera=`echo $linea | sed s/Nivel:.*// | sed s/[^:]*:\ //`
						carrera=`echo $linea | sed s/\ *Nivel.*// | sed s/[^:]*:\ //`
						;;

					"Situaci" )
						estado=`echo $linea | sed s/\ *Admisin.*// | sed s/[^:]*:\ //`

						admision=`echo $linea | sed s/.*Admisin// | sed s/ltimo.*// `

						ultimo=`echo $linea | sed s/.*ciclo//`


						;;

					" Centro" )

						centro=`echo $linea | sed s/Sede:.*// | sed s/[^:]*:\ //` 
						echo "insert into siiau_alumno_carrera values('$codigo','$nombre','$carrera','$estado','$admision','$ultimo','$centro'); "\
						| sed s/'|'//g | sed s/' '*\',\'' '*/\',\'/g \
						>> $fecha/alumnos.sql
                        echo "" >> $fecha/calificaciones.sql
                        echo -n "insert into calificacion values " >> $fecha/calificaciones.sql

						;;

					"Carrera" )
						edo="CAL"
						;;

				esac

				;;

			# Lee las calificaciones del estudiante
			"CAL" )
				# depuracion
				# echo $linea

				#echo -n '.'
				# elimina los campos inecesarios


				IFS=" "
				linea=`echo $linea | cut -f2,3,4,5,7,8,9,10 -d\|  | sed s/' '//g | sed s/===[^=]*===//g `

				IFS="|"


				# quiron 5:
				# Parche: hay registros que no tienen calificacion,
				# hay una materia que abre un parentesis y no lo cierra
				if [ "$(echo $linea | wc -w )" == "7" ] ; then
					echo $linea >> $fecha/calificaciones_ERROR.txt
					continue
				fi

				echo -n "('$codigo'" >> $fecha/calificaciones.sql
				for campo in  $linea ; do
				    echo -n ,"'$campo'"
				done | sed s/'|'//g | sed s/' '*\',\'' '*/\',\'/g  \
					| sed s/"\/ENE\/"/-01-/ \
					| sed s/"\/FEB\/"/-02-/ \
					| sed s/"\/MAR\/"/-03-/ \
					| sed s/"\/ABR\/"/-04-/ \
					| sed s/"\/MAY\/"/-05-/ \
					| sed s/"\/JUN\/"/-06-/ \
					| sed s/"\/JUL\/"/-07-/ \
					| sed s/"\/AGO\/"/-08-/ \
					| sed s/"\/SEP\/"/-09-/ \
					| sed s/"\/OCT\/"/-10-/ \
					| sed s/"\/NOV\/"/-11-/ \
					| sed s/"\/DIC\/"/-12-/ \
					| sed s/"\([0-9][0-9]\)-\(..\)-\(....\)"/"\3-\2-\1"/g \
					| sed s/"ORDINARIO"/ORD/ \
					| sed s/"EXTRAORD"/EXT/ \
					>> $fecha/calificaciones.sql

				echo  -n ")," >> $fecha/calificaciones.sql

				;;
		esac


	done


done
#cp siiau.sql $fecha/

echo
# Elimina los caracteres no imprimibles de todos los archivos
echo "Eliminando caracteres no imprimibles $(date)"

# Eliminar los caracteres que aparecen en el EDO del estudiante
#sed -i 's/[^[:print:]]//g' $fecha/*.sql # No funcionó
#sed -i "s/[^A-Za-z0-9 [:punct:]]//g" $fecha/*.sql
LANG=C sed  -i "s/[^A-Za-z0-9\ ()_.,'~';'-'#]//g"  $fecha/calificaciones.sql* $fecha/alumnos.sql

sed -i 's/insert into calificacion values $//'  $fecha/calificaciones.sql*

# Trim, elimina espacios en blanco al inicio y final de una cadena de comilla simple
# TAREA: con esto no se requiere usar TRIM()
sed -i "s/ \+'/'/g" $fecha/calificaciones.sql* $fecha/alumnos.sql
sed -i "s/' \+/'/g" $fecha/calificaciones.sql* $fecha/alumnos.sql


echo "Creando $fecha/alumnos.csv"
cat $fecha/alumnos.sql | sed 's/[\(\)]//g' | sed "s/[^']*'/'/"  | tr ';' ' ' > $fecha/alumnos.csv




echo "Corrigiendo el final de linea en calificaciones.sql " $(date)
# Las dos formas siguientes funcionan en diferentes implementaciones de sed
sed -i 's/"),$"/");"/' $fecha/calificaciones.sql
sed -i "s/),$/);/" $fecha/calificaciones.sql

cd $fecha
echo Segmentando el archivo calificaciones.sql
# Primero agrega los nuevos a los descargados anteriormente
cat calificaciones.sql.a* >> calificaciones.sql
if $(split -nl/5 calificaciones.sql  calificaciones.sql.) ; then
    rm calificaciones.sql
fi
cd ..

fi # if $CALIFICACIONES

echo "Fin " $(date)
kill $HIJOPID

