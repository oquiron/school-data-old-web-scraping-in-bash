# Oscar Pérez
# 2014-04-19
# Este archivo requiere que la base de datos llamada 'siiau'este creada y tenga permisos de creación.
# Descarga las materias pertenecientes a una carrera, esto es para obtener el area de cada materia.

carreras="ADM AFS AGP AGPP CPU CPUA DECO DEGE DEHA DEIN DUCA DUNE DUTI EAIM ECO IEC IGP LAFI LAGP LCOP LECO LGEA LIAD LIGA LIGE LIME LINI LIRH LTIN MAEC MAER MAPO MATI MEEC \
MER MIAD MIAN MIAP MIAU MICO MIDE MIDI MIDM MIEI MIEO MIFE MIFN MIGL MIGO MIGP MIGS MIGU MIME MIMP MIMR MINE MINI MINT MIRE MITA MITB MITC MITI MITL MITN MITO MITP MITR MPPG \
MTAD MTAG MTAI MTDI MTEM NIN PGHI PRT PSE RDNE RHU RMAD RMAS RMAU RMCO RMFI RMIM RMNE RMPE RXFI SIN TUCA TUR TURA TURI XCE "

carreras="ADM AFS AGPP CPUA ECO MER NIN LIGE RHU SIN TUR LAFI \
          LAGP LIAD LIGA LINI LIRH LIME LECO LCOP TURI LTIN TURA LGEA "

carreras="IELC"

# Descargar las materias de cada carrera
rm materia_carrera.csv

for carrera in $carreras ; do
	echo $carrera
	wget  http://iasv2.siiau.udg.mx/wco/scpcata.cataxcarr \
		--post-data="carrerap=$carrera&ordenp=2&mostrarp=10000&tipop=D" -O - \
	| sed 1d \
	| sed s/^/\"$carrera\",/ \
	>> materia_carrera.csv

done

# Crear el archivo sql

# Este es el encabezado que entrega SIIAU, es Incorrecto!!!! los campos correctos se describen en la tabla siiau_materia_carrera
# SUBJ,CLAVE,MATERIA,CRED,TEORIA,PRACTICA,TIPO,NIVEL,PRERREQUISITOS,CORREQUISITOS,CARRERAS,AREA,DEPTO


cat << EOF > materia_carrera.sql

drop table if exists siiau_materia_carrera;

create table siiau_materia_carrera (
	carrera char(20),
	subj char(50),
	clave char(10),
	nombre char(250),
	creditos int(2),
	teoria  int(2),
	practica int(2),
	tipo 	char(10),
	nivel	char(10),
	extraordinario char(2),
	prerrequisitos char(100),
	correquisitos char(100),
	area char(20),
	departamentos char(100)
);


EOF


cat materia_carrera.csv \
	| sed s/^/"insert into siiau_materia_carrera values ("/ \
	| sed s/$/");"/ \
	| sed s/,,/,'""',/ \
        | sed s/,,/,'""',/ \
        | sed s/,,/,'""',/ \
        | sed s/,,/,'""',/ \
        | sed s/,,/,'""',/ \
	| sed s/,\)/,'""'\)/ \
>> materia_carrera.sql


# TODO:
# Crear las tablas materia y materia_carrera apartir de siiau_materia_carrera


echo materia_carrera.sql creado!!!

