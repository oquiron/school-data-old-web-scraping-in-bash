#NRC,ST,DEPARTAMENTO,AREA,CLAVE,MATERIA,SECC,CRED,CUPO,OCUP,DISP,INI,FIN,L,M,I,J,V,S,EDIF,AULA,PROFESOR,FECHA INICIO,FECHA FIN,NIVEL

cat << EOF > oferta_2014a.sql
create table if not exists siiau_oferta (
    calendario char(10),
    nrc char(6),
    estatus char(1),
    departamento char(255),
    area    char(10),
    clave   char(5),
    materia char(255),
    secc    char(6),
    creditos    int(2),
    cupo        int(3),
    ocup   int(3),
    disp    int(3),
    ini     int(6),
    fin     int(6),
    l       char(1),
    m       char(1),
    i       char(1),
    j       char(1),
    v       char(1),
    s       char(1),
    edif    char(10),
    aula    char(10),
    profesor    char(255),
    fecha_ini   date,
    fecha_fin   date,
    nivel       char(6) 
);

EOF

cat cucea_oferta_2014a.csv \
    | sed 1d \
    | sed s/^/"insert into siiau_oferta values(\"2014A\","/ \
    | sed s/$/");"/ \
    | sed s/,,/,'""',/ \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/g \
        | sed s/,,/,'""',/ \
        | sed s/,,/,'""',/ \
    | sed s/,\)/,'""'\)/ \
    >> oferta_2014a.sql



