# Oscar Pérez 2014-04
# Reconstruido del de 2008
# Funciona 2014-04-29 v1.0
# 2014-04-30 Calcula el semestre_real


#######################
# Prerrequisitos


# Antes de ejecutar éste guión se deben tener los archivos sql necesarios, todos en un mismo directorio.
/*
El archivo de calificaciones.sql puede ser demasiado grande, hay que particionarlo:
split -nl/5 calificaciones.sql  calificaciones.sql.
*/
# La descarga de los datos de siiau se realiza con:
#     historial.sh     ->  alumnos.sql, calificaciones.sql y inscripcion-*.sql 
#     materia_carrera.sh -> materia_carrera.sql

# Los siguientes no son descargados de siiau, deben de ser creados a mano.
#     rutas-cucea2014.sql    # Incluye la ruta sugerida por cada carrera
#     semestres.sql         # Mapa de calendarios para hacer operaciones con ellos.

/*
 cat Rutas-cucea2014.csv | sed 's/[^[:print:]]//g'| sed s/' '//g \
 | sed s/^/"insert into ruta_materia values("/ | sed s/$/");"/ | sed 1d > rutas-cucea2014.sql 

# Agregar el campo carrera ( si no lo tiene )
alter table ruta_materia add carrera char(10) first;                                                            
update ruta_materia set carrera=substring(ruta,1,length(ruta)-1);                                               


*/ 
# Configurar mysql-server para trabajo pesado, funciona en Debian, no en ubuntu:

#zcat /usr/share/doc/mysql-server-5.5/examples/my-innodb-heavy-4G.cnf.gz  > /etc/mysql/my.cnf
/*

create database siiau2014a;
grant all on siiau2014a.*  to quiron@'%' identified by '13243546';

*/
# cargar éste guion:

# time mysql -u quiron -p123456 siiau2014a < siiau2014a-crear.sql 

######################
# Catalogo de Materias

select now() as 'Hora de inicio';
\! echo 'Creando el catalogo de materias'

source materia_carrera.sql;
source materia_carrera_corregido.sql

/*
        # Las areas de formacion se corrigieron en materia_carrera_corregido.sql
        create table materia_carrera 
        engine = InnoDB as
        select clave,carrera,area
        from siiau_materia_carrera
       
        ;

        alter table materia_carrera add primary key (carrera,clave);
*/

        create table materia  
        engine = InnoDB as
        select distinct clave,nombre,creditos,teoria,practica,tipo,nivel,subj
        from siiau_materia_carrera

        ;


        create table materia_requisito engine=InnoDB 
        as
        select * from (

                select carrera,clave,'PRE' as tipo,substr(prerrequisitos,2,4) as area,substr(prerrequisitos,7,5) as requisito
                from siiau_materia_carrera

                union

                select carrera,clave,'PRE' as tipo,substr(prerrequisitos,13,4) as area,substr(prerrequisitos,18,5) as requisito
                from siiau_materia_carrera

                union

                select carrera,clave,'PRE' as tipo,substr(prerrequisitos,24,4) as area,substr(prerrequisitos,29,5) as requisito
                from siiau_materia_carrera

                union

                select carrera,clave,'PRE' as tipo,substr(prerrequisitos,35,4) as area,substr(prerrequisitos,40,5) as requisito
                from siiau_materia_carrera

        ) as requisitos
        where requisito !=''


;

# Ajusta los requisitos de TURI y TURA
source turi_tura.sql


#Requisitos en arreglo:
create temporary table requisitos_arreglo  engine=InnoDB as
select clave,carrera,
substr(prerrequisitos,2,4) as area1,substr(prerrequisitos,7,5) as requisito1,
substr(prerrequisitos,13,4) as area2,substr(prerrequisitos,18,5) as requisito2,
substr(prerrequisitos,24,4) as area3,substr(prerrequisitos,29,5) as requisito3,
substr(prerrequisitos,35,4) as area4,substr(prerrequisitos,40,5) as requisito4

    from siiau_materia_carrera

;



#########################
# Rutas

\! echo 'Cargando rutas'

create table ruta_materia (
	ruta  char(20),
	semestre int(2),
	clave char(10),
    primary key (ruta,semestre,clave)

) engine = InnoDB
;


source rutas-cucea2014.sql ;

# Agregar el campo carrera ( si el archivo no lo tiene )
alter table ruta_materia add carrera char(10) first;
update ruta_materia set carrera=substring(ruta,1,length(ruta)-1);




create table calendario (
	num int(3),
	calendario char(5)
) engine=InnoDB 
;

source semestres.sql ;

#############################
# Cursos


# corregir historial.sh
\! echo 'Cargando calificaciones'
source calificaciones.sql.aa ;
source calificaciones.sql.ab ;
source calificaciones.sql.ac ;
source calificaciones.sql.ae ;
source calificaciones.sql.ad ;

# Esto debe modificars en historial.h
#alter table calificacion drop carreraA;

\! echo 'Cargando inscripcion'
source inscripcion-201310.sql;
source inscripcion-201320.sql;
source inscripcion-201410.sql;
source inscripcion-201410-inactivas.sql


alter table calificacion order by carrera,codigo,fecha;

# Historial.sh no esta hecho el cambio completo!!!
# Ajustes que no serán necesarios cuando se cambie historial.sh
# alter table siiau_registro change ciclo calendario char(10);
# alter table siiau_registro rename to siiau_inscripcion ;


create table inscripcion as select distinct codigo,carrera,calendario,nrc,clave from siiau_inscripcion;


###################################
# Alumnos
# historial.sh la tabla no se usa el nombre en insert into 

\! echo 'Cargando alumnos'
source alumnos.sql;
alter table siiau_alumno_carrera order by carrera,codigo;
create index siiau_alumno_carrera_indice  on siiau_alumno_carrera (carrera,codigo);


create table alumno engine=InnoDB as 
select distinct codigo,nombre from siiau_alumno_carrera

;



# Calcular el semestre_ultimo
\! echo 'Calculando semestre_ultimo'
create temporary table semestre_ultimo  engine=InnoDB as
select codigo,carrera,fin.num - ini.num + 1 as semestre_ultimo
from siiau_alumno_carrera,calendario as ini, calendario as fin 
where ini.calendario=ciclo_ingreso and fin.calendario=ciclo_ultimo 
        

;

alter table siiau_alumno_carrera add semestre_ultimo int(2) after ciclo_ultimo;

# Tardo 9 minutos!!!
update  siiau_alumno_carrera, semestre_ultimo
set siiau_alumno_carrera.semestre_ultimo=semestre_ultimo.semestre_ultimo
where siiau_alumno_carrera.carrera = semestre_ultimo.carrera
and siiau_alumno_carrera.codigo = semestre_ultimo.codigo

;

drop table semestre_ultimo;

# Crear la tabla carrera
create table carrera  engine=InnoDB  as 
select distinct trim(replace(substring(carrera,locate('=',carrera),7),'=',' ')) as clave, 
substring(carrera,1,locate('=',carrera)-1) as nombre 
from siiau_alumno_carrera;

update siiau_alumno_carrera set carrera=trim(replace(substring(carrera,locate('=',carrera),7),'=',' ')) ;
alter table siiau_alumno_carrera change carrera carrera char(10);

##########################################
# Alumno Trayectoria

\! echo 'Calculando alumno_historial = calificaciones + semestre_real + inscripcion 2014a'
# Los calendarios de verano se consideran el mismo semestre que el calendario A anterior.
set @codigo='';
set @sem=0;
set @cal='';

create table alumno_historial  engine=InnoDB as
select codigo,carrera,calendario,nrc,clave,calificacion,tipo,creditos,fecha,semestre_real 
from
(
   select *, 
            if( @codigo = codigo ,
                if(calendario = @cal,
                    @sem,
                    if ( calendario like '%V',
                        @sem,
                        @sem:=@sem+1
                       )
                    ),
               @sem:=1
            ) 
            as semestre_real,

            @cal:=calendario ,
            @codigo := codigo



  from 


    (
    select codigo,carrera,calendario,nrc,clave,calificacion,tipo,creditos,fecha from
        (select * from calificacion
        order by codigo,calendario
        ) as calculo_real
    union
        (select codigo,carrera,calendario,nrc,clave,'','',0,''
        from inscripcion
        where calendario='2014A'

        )
    ) as unidas
    order by carrera,codigo,calendario
) as sin_orden
;

alter table alumno_historial order by carrera,codigo,fecha;

\! echo 'Tabla temporal historial_acreditadas'
create temporary table historial_acreditadas  engine=InnoDB as
select * from alumno_historial
where   creditos > 0 or tipo = ''
order by carrera,codigo,clave
    ;


# Parche para las carreras de Malu
source lgea_lige.sql


create index historial_indice  on historial_acreditadas (carrera,codigo,clave);

\! echo 'Creando la tabla temporal de alumno_ruta'
/*
create temporary table alumno_ruta as
  select codigo,ruta_materia.carrera,ruta,clave,semestre 
  from siiau_alumno_carrera, ruta_materia 
  where siiau_alumno_carrera.carrera = ruta_materia.carrera
order by carrera,codigo
 ;
*/

create temporary table alumno_ruta as 
select codigo,ruta_materia.carrera,ruta,clave,semestre
  from siiau_alumno_carrera, ruta_materia
  where siiau_alumno_carrera.carrera = ruta_materia.carrera
  and edo not like '%TITULADO%'
  and edo not like '%PASANTE%'
  and edo not like '%EGRESADO%'
  and edo not like '%GRADUADO ACTA DE TITULACION%'
  and edo not like '%ALUMNO EN PROCESO DE TESIS%'
  #and edo not like '%%'
  and centro like '%CENTRO UNIVERSITARIO DE CIENCIAS ECONOMICO-ADMINISTRATIVAS%'
order by carrera,codigo
 ;
 
create index alumno_ruta_indice on alumno_ruta (codigo,clave);
########################################################################
########################################################################
\! echo 'Procesando por carrera'
delimiter |
drop procedure if exists crear_proyeccion |
create procedure crear_proyeccion ()
    NOT DETERMINISTIC
    SQL SECURITY DEFINER
    COMMENT ''
begin
    declare vcarrera char(10);
    declare n int;
    declare terminar bool default false;

    declare carreras cursor for select distinct carrera from ruta_materia;
    #select clave from carrera;


    DECLARE CONTINUE HANDLER
    FOR SQLSTATE '02000'
    SET terminar = TRUE;

    drop table if exists alumno_trayectoria;

    

    open carreras;
    repeat      
        fetch carreras into vcarrera;
        
        select vcarrera, now();



        # select '  1. Caculando la trayectorias' as ' ';
        create table if not exists alumno_trayectoria (
            codigo  char(12),
            carrera char(10),
            ruta    char(20),
            clave   char(5),
            semestre int(2),
            semestre_real int(2),
            calendario char(5),
            nrc char(6),
            calificacion int(3),
            tipo    char(20),
            creditos int(2),
            fecha   date
        ) engine=InnoDB;
      
        insert into alumno_trayectoria
            select alumno_ruta.*,c.semestre_real,c.calendario,c.nrc,c.calificacion,c.tipo,c.creditos,c.fecha
            from (select * from alumno_ruta where carrera = vcarrera ) as alumno_ruta
            left join
                 (select * from historial_acreditadas where carrera = vcarrera) as c
            using (carrera,codigo,clave)
        ;
        
    
        ######################################################################################

        # select '  2. Agregando Optativas y Selectivas que no éstan en la ruta' as ' ';
        insert into alumno_trayectoria
            select codigo,historial_acreditadas.carrera,ruta,historial_acreditadas.clave,semestre,
                historial_acreditadas.semestre_real,calendario,nrc,calificacion,tipo,creditos,fecha
            from  ( select * from historial_acreditadas where carrera = vcarrera ) as historial_acreditadas
            left join ruta_materia
            on ( historial_acreditadas.clave = ruta_materia.clave 
                 and ruta like concat(historial_acreditadas.carrera,'%'))
            where  semestre is NULL
        ;
        #######################################################################################
        
        # select '  3. Creando Abanico sin validacion' as ' ';
        # create temporary table alumno_abanico_sin_validacion  engine=InnoDB as
        create temporary table if not exists alumno_abanico_sin_validacion (
            codigo char(10),
            carrera char(5),
            ruta char(10),
            clave char(5),
            semestre int (2),
            semestre_real int(2),
            semestre_ultimo int(2),
            requisito1 char(5),
            requisito2 char(5)
            
        ) engine = InnoDB;
        
        insert into alumno_abanico_sin_validacion 
        select distinct abanico.*, r.requisito1, r.requisito2 from
        (select alumno_trayectoria.codigo,alumno_trayectoria.carrera,ruta,clave,semestre,
                 alumno_trayectoria.semestre_real,semestre_ultimo 


            from alumno_trayectoria, siiau_alumno_carrera
            where
            -- No ha cursado la materia
            calendario is null 
            -- Este programada en un semestre anterior al que va el estudiante
            and semestre <=  semestre_ultimo + 1
            and alumno_trayectoria.codigo  = siiau_alumno_carrera.codigo 
            and alumno_trayectoria.carrera = siiau_alumno_carrera.carrera 

            #and (      siiau_alumno_carrera.edo like 'ACTIVO%'
            #    or siiau_alumno_carrera.edo like '%ARTICULO 34%'
            #    )
            # 2:
            and alumno_trayectoria.carrera = vcarrera


            ) as abanico
        left join
        # Numero de prerrequisitos
        ( select clave,carrera,requisito1,requisito2 
          from requisitos_arreglo 
          # No se restrigio a vcarrera
          ) as r
        on abanico.clave = r.clave
        and r.carrera = r.carrera

        ;

        ######################################################################################
        
        # select '  4. Abanico 1requisito'  as  ' ' ;
        #create temporary table abanico_1requisito  engine=InnoDB as
        create temporary table if not exists abanico_1requisito (
            codigo char(10),
            carrera char(10),
            ruta     char(10),
            clave    char(5),
            semestre    int(2),
            semestre_real     int(2),
            semestre_ultimo int(2),
            requisito1    char(5),
            acreditada  bool,
            requisito2    char(5)
        );
        insert into abanico_1requisito
            select    
                abanico.codigo,
                abanico.carrera,
                abanico.ruta,
                abanico.clave,
                abanico.semestre,
                abanico.semestre_real,
                abanico.semestre_ultimo,
                abanico.requisito1,
                acreditadas.acreditada ,
                abanico.requisito2
            from
                (select * from alumno_abanico_sin_validacion 
                #3:
                where carrera = vcarrera
                )
                as abanico

                left join
                # 2014-0515
                (select codigo, carrera, clave, if(creditos>0 or tipo='' ,true,false) as acreditada 
                from alumno_trayectoria 
                # 3:
                where carrera = vcarrera
                )
                as acreditadas

                on  abanico.requisito1 <> ''
                and abanico.carrera     = acreditadas.carrera
                and abanico.codigo    = acreditadas.codigo
                and abanico.requisito1 = acreditadas.clave
                

        ;
        ######################################################################################
        #select '  5: Abanico Final ' as ' ' ;

        #create table alumno_abanico  engine=InnoDB as
        create  table  if not exists alumno_abanico (
            codigo char(10),
            carrera char(10),
            ruta     char(10),
            clave    char(5),
            semestre    int(2),
            semestre_real     int(2),
            semestre_ultimo int(2),
            requisito1    char(5),
            acreditada1  bool,
            requisito2    char(5),
            acreditada2  bool
        ) engine = InnoDB;
        
        insert into alumno_abanico
        select 
            abanico2.codigo, 
            abanico2.carrera, 
            abanico2.ruta,
            abanico2.clave, 
            abanico2.semestre, 
            abanico2.semestre_real,
            abanico2.semestre_ultimo, 
            abanico2.requisito1, 
            if( isnull(abanico2.acreditada),'',abanico2.acreditada)  as acreditada1,
            abanico2.requisito2,
            if( isnull(acreditadas2.acreditada),'',acreditadas2.acreditada) as acreditada2
            

         from
            ( select * from abanico_1requisito 
				# 4
				where carrera = vcarrera
            )
            as abanico2
                
            left join
            # 2014-0515
            (select codigo, carrera, clave, if(creditos>0 or tipo ='',true,false) as acreditada from alumno_trayectoria
               # 4
				where carrera = vcarrera
            ) 
            as acreditadas2
            
            on  abanico2.requisito2 <> ''
            and abanico2.carrera    = acreditadas2.carrera
            and abanico2.codigo     = acreditadas2.codigo
            and abanico2.requisito2 = acreditadas2.clave

        ;

		update alumno_abanico set acreditada1=null where requisito1='';     
		update alumno_abanico set acreditada2=null where requisito2='';     
		
        ######################################################################################
    until terminar end repeat;
    #select * from resultado;
    select now() as 'Hora Final';
end;
|


delimiter ;

call crear_proyeccion();


alter table alumno_trayectoria order by carrera,codigo,semestre;

create index alumno_trayectoria_indice on alumno_trayectoria (carrera,codigo);


\! echo 'Terminado!'
select 'Hora Final', now();
quit

    
